# Advanced Bioinformatics FA15

This repository contains the documentation, datasets, and assignments for Dan Barshis' Advanced Bioinformatics course (Biology 795-895) at Old Dominion University, as taught in the Fall of 2015.

See the [syllabus](https://bitbucket.org/dbarshis/15odu_advbioinf/src/master/syllabus/Biol795-895_AdvBioinf_Fall_2015_Barshis_v1.pdf) for additional information.

There is no required textbook for the class. A recommended resource is :

Haddock, S. H. D. and Dunn, C. W. (2010). Practical Computing for Biologists. 
[Sinauer Associates](http://practicalcomputing.org). Recommended.

Here are some of the appendices from the book, which summarize frequently used 
commands:
[Appendices](http://practicalcomputing.org/files/PCfB_Appendices.pdf)

## Course description

This course is designed to teach students the various steps involved in analyzing next-generation sequencing data for gene expression profiling and polymorphism identification and analyses. The class will be analyzing a publication worthy dataset in hopes of generating publishable analyses that will be of sufficient quality to withstand peer-review. The class will follow a workshop setting with a combination of lectures, paper discussions, and instructor and student lead programming sessions.

## Learning Objectives

1. Students will become comfortable using the command line and campus cluster to perform bioinformatics analyses
2. Students will be able to conduct the follow analyses using next generation sequence data:

- De novo assembly of a transcriptome
- Taxon assignment of assembled contigs
- Annotation of assembled contigs
- Differential gene expression analyses using mRNA sequencing
- Polymorphism detection and outlier analyses of sequence data (mRNA or gDNA)

## Ground rules

- Never move things from the shared classdata directory, only copy.
- No spaces in filenames or paths
- Don't run things on the head node
- Start with interactive jobs, then move to submitted jobs
- Don't use more processors than you've requested
- Don't run with scissors

## Advice

- DuckDuckGo it
- man
- copy and paste, don't type
- tab complete
- check your end of line characters
- the command line is never wrong, it's just very very literal
- DuckDuckGo it

## Qsub script header

[qsubheader](https://bitbucket.org/dbarshis/15odu_advbioinf/src/master/qsubheader.txt)

## Useful commands
- qsub #for submitting jobs
- qrsh #for starting an interactive job
- qstat #for checking job status, do qstat -u yourusername to look at only your jobs
- chmod #changing permissions for scripts