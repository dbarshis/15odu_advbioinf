## Ground rules

- Never move things from the shared classdata directory, only copy.
- No spaces in filenames or paths
- Don't run things on the head node
- Start with interactive jobs, then move to submitted jobs
- Don't use more processors than you've requested
- Don't run with scissors
